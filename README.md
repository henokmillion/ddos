# DDOS
## Description
This is an application that launches a DOS attack on a specified url.
## Running the application
To run this application: start the application from `Main.java`
* enter a `url` of the client you want to target
* enter the amount of __threads__ 


## How it works
This application is made with Java programming language.
It takes advantage of `Thread` and `HttpURLConnection` libraries by
creating (multiple) threads and sending an HTTP request to the 
specified client.
**Note:** Running this application on multiple computers and increasing the __thread amount__ is recommended for better output.
