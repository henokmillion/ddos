package com.company;

import java.util.Scanner;

public class Main {

    private static String mTARGET_URL;
    private static int mTHREAD_COUNT;
    public static void main(String[] args) throws Exception {
        System.out.print("Please enter the target url: ");
        Scanner scanner = new Scanner(System.in);
        mTARGET_URL = scanner.nextLine();
        System.out.print("Thread Count: ");
        mTHREAD_COUNT = Integer.parseInt(scanner.nextLine());
        for (int i = 0; i < mTHREAD_COUNT; i++) {
            ThreadManager threadManager = new ThreadManager(mTHREAD_COUNT, mTARGET_URL);
            threadManager.start();
        }
    }
}
