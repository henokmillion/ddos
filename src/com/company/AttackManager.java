package com.company;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by User on 3/28/2018.
 */
public class AttackManager {
    private static URL mTARGET_URL;

    public AttackManager(String targetUrl) throws MalformedURLException {
        mTARGET_URL = new URL(targetUrl);
    }

    public void attack() throws IOException {
        HttpURLConnection connection = (HttpURLConnection) mTARGET_URL.openConnection();
        connection.setDoOutput(true);
        connection.setDoInput(true);
        connection.setRequestMethod("GET");
        connection.setRequestProperty("charset", "utf-8");
        connection.setRequestProperty("Host", "localhost");
        connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:8.0) Gecko/20100101 Firefox/8.0");
        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        System.out.println(this + " " + connection.getResponseCode());
        connection.getInputStream();
    }
}
