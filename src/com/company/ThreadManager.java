package com.company;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by User on 3/28/2018.
 */
public class ThreadManager extends Thread {
    private static int mTHREAD_COUNT;
    private static String mTARGET_URL;
    private AtomicBoolean running = new AtomicBoolean(true);

    public ThreadManager(int threadCount, String targetUrl) {
        mTHREAD_COUNT = threadCount;
        mTARGET_URL = targetUrl;
    }

    @Override
    public void run() {
        while(this.running.get()) {
            try {
                attack();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void attack() throws Exception {
        AttackManager attackManager = new AttackManager(mTARGET_URL);
        attackManager.attack();
    }
}
